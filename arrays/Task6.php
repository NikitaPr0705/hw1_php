<?php
$arr = array(11, 8, 10, 3, 1, 5, 6, 7, 16, 1);

function reverseArr($arr) {
    $arrCopy = array();
    for($i = count($arr) - 1; $i >= 0; $i--) {
        $arrCopy[$i] = $arr[$i];
    }
    return $arrCopy;
}
print_r(reverseArr($arr));
?>
