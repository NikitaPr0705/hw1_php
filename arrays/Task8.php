<?php
$arr = array(1, 2, 3, 4, 5, 6, 7);
function reverseHalfArr($arr) {
    $arrCopy = $arr;
    if(count($arr) % 2 === 0) {
        $halfArr = count($arrCopy) / 2;
        for($i = 0; $i < $halfArr; $i++) {
            $arrTemp = $arrCopy[$i];
            $arrCopy[$i] = $arrCopy[$halfArr + $i];
            $arrCopy[$halfArr + $i] = $arrTemp;
        };
        } else {
            $middle = (count($arrCopy) - 1) / 2;
            for ($i = 0; $i < $middle; $i++) {
            $arrTemp = $arrCopy[$i];
            $arrCopy[$i] = $arrCopy[$middle + $i + 1];
            $arrCopy[$middle + $i + 1] = $arrTemp;
            };
        }
    return $arrCopy;
};
    print_r(reverseHalfArr($arr));
?>
