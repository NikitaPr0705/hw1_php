<?php
$arr = array(3, 5, 1, 7, 2, 6, 4);
function bubbleSort($arr) {
   for($i = 0, $length = count($arr) - 1; $i < $length; $i++) {
       for($j = 0, $lastIndex = $length - $i; $j < $lastIndex; $j++) {
            if($arr[$j] > $arr[$j + 1]) {
                $swap = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $swap;
            }
       }
   }
   return $arr;
};

print_r(bubbleSort($arr));


function selectSort($arr) {
    $length = count($arr);
    $lastIndex = $length - 1;
    for($i = 0; $i < $lastIndex; $i++) {
        $minIndex = $i;
        for($nextIndex = $i + 1; $nextIndex < $length; $nextIndex++) {
            if($arr[$minIndex] > $arr[$nextIndex]) {
                $minIndex = $nextIndex;
            }
        }
        if($minIndex !== $i) {
            [$arr[$i], $arr[$minIndex]] = [$arr[$minIndex], $arr[$i]];
        }
    }
    return $arr;
    print_r($arr);
}

print_r (selectSort($arr));

function insertionSort($arr) {
    // для каждого $a[$i] начиная со второго элемента...
    for ($i = 1; $i < count($arr); $i++) {
        $currentElem = $arr[$i];
        $previousIndex = 0;
        for ($previousIndex = $i - 1; $previousIndex >= 0 && $arr[$previousIndex] > $currentElem; $previousIndex--) {
            $arr[$previousIndex + 1] = $arr[$previousIndex];
        }
        $arr[$previousIndex + 1] = $currentElem;
    }
    return $arr; // return
}
print_r (insertionSort($arr));

?>
