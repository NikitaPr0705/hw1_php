<!DOCTYPE html>
<html lang = "en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HW1 PHP</title>
</head>
<body>
<?php
    echo "<h3>IfElse</h3>";
    echo '<br><a href="ifElse/task1.php">Task1</a>';
    echo '<br><a href="ifElse/task2.php">Task2</a>';
    echo '<br><a href="ifElse/task3.php">Task3</a>';
    echo '<br><a href="ifElse/task4.php">Task4</a>';
    echo '<br><a href="ifElse/task5.php">Task5</a>';
?>
<hr>
<?php
echo "<h3>Arrays</h3>";
echo '<br><a href="arrays/task1.php">Task1</a>';
echo '<br><a href="arrays/task2.php">Task2</a>';
echo '<br><a href="arrays/task3.php">Task3</a>';
echo '<br><a href="arrays/task4.php">Task4</a>';
echo '<br><a href="arrays/task5.php">Task5</a>';
echo '<br><a href="arrays/task6.php">Task6</a>';
echo '<br><a href="arrays/task7.php">Task7</a>';
echo '<br><a href="arrays/task8.php">Task8</a>';
echo '<br><a href="arrays/task9.php">Task9</a>';
echo '<br><a href="arrays/task10.php">Task10</a>';
?>
<hr>
<?php
echo "<h3>Functions</h3>";
echo '<br><a href="functions/task1.php">Task1</a>';
echo '<br><a href="functions/task2.php">Task2</a>';
echo '<br><a href="functions/task3.php">Task3</a>';
echo '<br><a href="functions/task4.php">Task4</a>';
echo '<br><a href="functions/task5.php">Task5</a>';
echo '<br><a href="functions/task6.php">Task6</a>';
?>
<hr>
<?php
echo "<h3>Loops</h3>";
echo '<br><a href="loops/task1.php">Task1</a>';
echo '<br><a href="loops/task2.php">Task2</a>';
echo '<br><a href="loops/task3.php">Task3</a>';
echo '<br><a href="loops/task4.php">Task4</a>';
echo '<br><a href="loops/task5.php">Task5</a>';
echo '<br><a href="loops/task6.php">Task6</a>';
?>
</body>
</html>