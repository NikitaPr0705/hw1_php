<?php
$rate = 50;
function countRate($rate) {
    if ($rate >= 0 && $rate <= 19) {
        return 'F';
    } else if ($rate >= 20 && $rate <= 39) {
        return 'E';
    } else if ($rate >= 40 && $rate <= 59) {
        return 'D';
    } else if ($rate >= 60 && $rate <= 74) {
        return 'C';
    } else if ($rate >= 75 && $rate <= 89) {
        return 'B';
    } else if ($rate >= 90 && $rate <= 100) {
        return 'A';
    } else if ($rate > 100) {
        return 'Error, there is too many';
    } else if ($rate < 20) {
        return 'Error, there number is too small';
  }
}
echo countRate($rate);
?>