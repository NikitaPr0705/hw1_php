<?php
$first = "first quarter";
$second = "second quarter";
$third = "third quarter";
$fourth = "fourth quarter";
$x = 2;
$y = 5;

function whichQuarter($x, $y) {
    if($x > 0 && $y > 0) {
        return "first quarter";
    }
    if ($x < 0 && $y > 0) {
        return "second quarter";
    }
    if ($x < 0 && $y < 0) {
        return "third quarter";
    }
    if ($x > 0 && $y < 0) {
        return "fourth quarter";
    }
}
echo whichQuarter($x, $y);
?>