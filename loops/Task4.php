<?php
$num = 5;

function countFact($num) {
    $factResult = 1;
    for ($i = 1; $i <= $num; $i++) {
        $factResult *= $i;
    }
    return $factResult;
}

echo countFact($num);
?>